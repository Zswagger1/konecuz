# Stopky



## Obsah
    Stopky jsou řízené přes STM8 Nucleo, kde maximální hodnota počítání 59:59 neboli 59 minut a 59 sekund.
    Tento projekt jsem realizoval na nepájivém poli tak, že na výstupu jsou čtyři 7segmentové displeje ovladané přes 4 BCD dekodéry. Stopky se dají také tlačítkem pozastavit a následně resetovat

## Blokové schéma
```mermaid
    graph TD;
    STM8-->1.dekodér-->1.sedmisegment;
    STM8-->2.dekodér-->2.sedmisegment;
    STM8-->3.dekodér-->3.sedmisegment;
    STM8-->4.dekodér-->4.sedmisegment;
    PC-->STM8
```
## Seznam součástek
| Typ součastky | Hodnota | Počet kusů |  Cena za kus |  Cena dohromady |
|:-------------:|:-------:|:----------:|:------------:|:---------------:|
| STM8 Nucleo   |         |      1     |     250Kč    |       250Kč     |
| 7seg. displej |         |      4     |      8Kč     |       32Kč      |
|    Rezistor   |   270R  |     28     |      2Kč     |       56Kč      |
|     Propojovací drátky   |         |     64     |      1Kč     |       64Kč      |
| Dekodér E147D |         |      4     |     25Kč     |      100Kč      |
| Celkem:       |         |            |              |      502Kč      |

## Zapojení
![Zapojení na nepájívém poli](stopzap.jpg)

## Kicad schéma
![Schémá zapojení v KiCadu](stopky_ah.PNG)

## Zdrojový kód
    Zdrojový kód je součástí přílohy.

## Závěr
    Dělání tohoto projektu pro mě nebylo úplně nejjednodušší. Nejprve jsem to měl udělané úplně jinak, ale po velice dlouhé konzultaci a pomoci od spolužáků jsem to předělal a myslím si, že je to teď lepší. Dále jsem měl problém se samotným gitlabem, který mi z nějakého důvodu nedovoloval nahrát některé soubory. Celkově si ale myslím, že jsem tento projekt relativně zvládl a jsem také rád, že už to mám za sebou.

## Autor
  Jan Směšný
